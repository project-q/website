---
title:  "Zielgruppenanalyse"
layout: post
date:   2020-03-17 12:00:00
categories: microservices analysis
badges:
 - type: primary
   tag: th-koeln
 - type: primary
   tag: analysis
---


Das veröffentliche Projekt soll verschiedene Zielgruppen ansprechen. Zum einen sollen freie Programmierer angesprochen werden, die Interesse haben am Projekt weiterzuarbeiten, um die Fertigstellung des gesamten Webshops anzustreben. Zum anderen sollen Nutzer angesprochen werden, die das System testen. Da die einfache Einbindung des Systems zur Zielsetzung des Projekts gehört, sollen Nutzer gemeinsam mit der Dokumentation das System einfach aufsetzen und nutzen können. Diese Zielgruppe kann auf Plattformen, wie [Github](www.github.com) oder [Gitlab](www.gitlab.com) gefunden werden. 
Die Dokumentation ist in der Sprache deutsch verfasst und richtet sich somit an Nutzer innerhalb Deutschland bzw. mit deutschen Kenntnissen. 


***Wettbewerberanalyse***
Wettbewerber, wie [Sylius](https://sylius.com) bieten neben ihrem Open-Source Projekt verschiedene Onlinekurse an, um ihr System zu erlernen. Diese haben zur Zeit 550 Contributors. Ihr Zielgruppe setzt sich aus den beteiligten Programmierern und Unternehmen, die das System nutzen zusammen.

[MyOOS](https://github.com/r23/MyOOS) hingegen ist ein Community-Projekt von [r23](https://github.com/r23), der eine Shopsystem mit Basic Funktionen auf Github veröffentlicht hat. Das Projekt hat bisher nur 2 Contributors und bietet eigenen Support für dieses System.

[Magento](https://marketplace.magento.com/?_ga=2.52442536.1074671025.1583141370-2042716924.1583141370) bietet ebenfalls ein Open-Source-Community System an. Diese sprechen erfahrene Programmierer an, die das Basic Onlineshop-System nutzen und etablieren können und erhoffen sich durch den Verkauf von Support, Extensions und Themes auch die Nutzung von Unternehmen, die z.B. eine kurze Time-To-Market Zeit erreichen möchten.


Die Zielgruppe des Projekt Q richtet sich in der Markteintrittsphase zuerst an Programmierer, die Interesse haben, das Projekt weiter auszubauen. Außerdem sollen Nutzer das System testen um Verbesserungsvorschläge und Fehlerbehebungen effizient zu finden. Dazu wird auf Gitlab das Projekt zusammen mit der Dokumentation veröffentlicht. Sobald alle Basic Funktionen und Issues des Webshops erfolgreich umgesetzt worden, sollen auch Unternehmen angesprochen werden, die das Projekt nutzen und selbst aufsetzen sollen. Die möglichen Businessmodelle werden im Kapitel [Businessmodelle](https://project-q.gitlab.io/website/2020/business-models/)
behandelt.