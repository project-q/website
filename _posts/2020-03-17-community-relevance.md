---
title:  "Relevanz der Community"
layout: post
date:   2020-03-17 12:00:00
categories: microservices community
badges:
 - type: primary
   tag: th-koeln
 - type: primary
   tag: community
---

Einer der wichtigsten Aspekte der Open-Source-Welt zeigt sich in der Community eines Projekts. Die Größe und der Aktivitätsgrad dieser Gemeinschaft hat erhebliche Auswirkungen auf die Organisation und den Reifegrad des zu erstellenden Systems. 

Was ist eine Open-Source-Community? Dazu gehören alle Produktentwickler, Benutzer, die an einer Teilnahme interessiert sind und alle anderen Personen, die sich an einem Projekt beteiligen möchten. Im Wesentlichen zeigt sich eine freilaufende Organisation aller, die sich ausverschiedensten Gründen für ein bestimmtes Projekt interessieren. Meist gibt es keine formalen Anforderungen oder Regeln für die Teilnahme. Eine Projekt-Community steht somit allen Teilnehmer offen. Mangelnde Formalität bedeutet jedoch, dass es keine Standards für die Teilnahme oder das Verhalten gibt. Für alle Interaktionen der Gemeinschaft gelten jedoch ungeschriebene Regeln. Von einem Gemeinschaftsmitglied wird erwartet, dass es sich respektvoll interagiert, dass es begründete Argumente dafür vorbringt, warum eine bestimmte Vorgehensweise richtig ist, und dass es vor allem einen Beitrag zur Gemeinschaft leistet und diese auch nutzt.
Solche Community bilden sich auf Websiten wie [Github](www.github.com). Interessierte können ihre Projekte dort veröffentlichen und auf die Teilnahme von anderen Nutzern hoffen, um ihr Projekt weiter auszubauen, zu testen oder sich bei der Fehlersuche helfen zu lassen.

Open-Source-Produktgemeinschaften sind sehr mächtig und bieten enorme Vorteile: Wie bereits beschrieben, macht die Interaktion zwischen Entwicklern und Anwendern ein nützlicheres Produkt aus. Das Produkt ist besser dafür geeignet, dass mehrere Leute seine Richtung bestimmen und seine Stabilität durch die Nutzung verbessern. Die Gemeinschaft bietet eine Ressource, auf die man sich für das Fachwissen über das Produkt stützen kann. Es kommt sehr häufig vor, dass sich die Mitglieder der Gemeinschaft gegenseitig bei der Lösung von Problemen helfen oder mögliche Lösungen für die Nutzung des Produkts vorschlagen.

Aufgrund der Notwendigkeit eines Konsenses und der großen Gemeinschaft kann das Tempo der Arbeit und der Entscheidungsfindung sehr langsam erscheinen. Es gibt kaum Möglichkeiten, dies zu umgehen, aber es verursacht normalerweise kein enormes Problem. Trotz der starken informellen Regeln der Beteiligung kommt es manchmal zu unreifem Verhalten. Dies kann in Form von Beschimpfungen geschehen. Gelegentlich führt die Gemeinschaft langwierige Diskussionen über Themen, die nicht zum Thema gehören. Ein einzigartiger Aspekt einer Open-Source-Community ist, dass sie im Wesentlichen anonym und extrem dezentralisiert ist. Fast die gesamte Interaktion erfolgt über E-Mail und Webforen. Es kommt häufig vor, dass sich die Mitglieder des Entwicklungsteams noch nie getroffen haben, aber recht eng zusammenarbeiten, um ein qualitativ hochwertiges Produkt zu schaffen. Viele Mitglieder einer Produktgemeinschaft arbeiten zusammen, um ein Problem erfolgreich zu lösen, und sind dabei buchstäblich über die ganze Welt verstreut.

Viele IT-Manager zögern anfangs, ein Produkt zu verwenden, das so stark von einer informellen Gemeindeorganisation abhängt. Sie sind der Meinung, dass Open-Source-Communities, denen es an hierarchischer Führung und einer formalen Verwaltungsstruktur mangelt, ineffizient und chaotisch sein müssen. Diese Reaktion ist leicht zu verstehen, da sich ein Großteil des Unternehmens- und Regierungslebens um Hierarchien und formale Verhaltensregeln dreht.
Im Grunde beruht diese Reaktion auf der Befürchtung, dass die Gemeinschaft bedeutet, dass niemand wirklich für das Produkt verantwortlich ist - dass es niemanden gibt, an den man sich wegen eines bestimmten Problems oder eines dringenden Bedarfs wenden kann. Es gibt ein beruhigendes Gefühl, dass ein kommerzieller Anbieter einen verantwortungsvollen Ansprechpartner bietet, der manchmal auf charmante Weise als "eine Kehle zum Ersticken" bezeichnet wird. Andererseits ist es keine Garantie für ein verantwortungsvolles Verhalten, wenn ein kommerzieller Anbieter hinter einem Produkt steht. 

Es gibt jedoch einen Präzedenzfall für eine Technologie, die von einer informell strukturierten Organisation erfolgreich umgesetzt wird. Es ist einer, den wahrscheinlich jeder Leser dieses Buches täglich nutzt: das Internet. Trotz des Fehlens einer formellen Hierarchie, der Verwendung des informellen Request for Comment (RFC)-Standards und des Vertrauens auf den persönlichen Ruf als Führungskraft hat das Internet einen brillanten Erfolg erzielt. Es ist unwahrscheinlich, dass eine einzige kommerzielle Einrichtung das geschaffen hätte, was diese informelle Organisation zustande gebracht hat. Viele Beobachter argumentieren sogar, dass sich das Internet aufgrund des Mangels an formaler Struktur und Vorschriften schneller entwickelt hat und nützlicher ist, als es sonst möglich gewesen wäre.

*[The Source of Open Source,  Succeeding with Open Source By Bernard Golden, Published Aug 10, 2004 by Addison-Wesley Professional. Part of the Addison-Wesley Information Technology Series series. ISBN-13: 978-0-321-26853-2]*
