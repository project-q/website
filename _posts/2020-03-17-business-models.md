---
title:  "Businessmodelle"
layout: post
date:   2020-03-17 12:00:00
categories: microservices business
badges:
 - type: primary
   tag: th-koeln
 - type: primary
   tag: business
---

Offene Technologien können zu kommerziellen Bedingungen zur Verfügung gestellt werden. Für die Vermarktung und Finanzierung von Open-Source Projekten können verschiedene Modelle betrachtet werden. 

**Dual-Lizensierung**
Dieses Lizenzierungsmodell ermöglicht einem Unternehmen neben der Entwicklung eines Open-Source Systems, den Verkauf einer Lizenz für das entwickelte System. Das System kann dabei open-source weiterentwickelt werden. Entwickler können durch den Verkauf von Lizenzen entlohnt werden. Die Entwicklung freier Software wird so gefördert und gleichzeitig kann ein professionelles Geschäftsmodell etabliert werden. Nachteile ergeben sich daraus, dass das lizensierte Unternehmen die Arbeit externer Open-Source-Entwickler ausnutzen könnte und die Weiterentwicklung des Systems ihrerseits stoppen kann, ohne weiteren Support für die Käufer zu bieten.


**Software-as-a-Service** (SaaS)
SaaS ist eine Methode der Softwarebereitstellung, die den Zugriff auf das System remote ermöglicht. In diesem webbasierten Modell hosten und warten Softwareanbieter der Open-Source-Software die Server, Datenbanken und den Code, aus denen eine Anwendung besteht. Das Cloud-basierte Modell ist inzwischen so verbreitet, dass mehr als 60% der Software-Suchenden, die Software-Beratung nutzen. Unternehmen, die das System nutzen möchten, müssen somit nur die Gestaltung des Shops und die Datenverwaltung über ein Interface verwalten.
Das traditionelle Modell der Softwarebereitstellung unterscheidet sich von SaaS in zwei wesentlichen Punkten:
SaaS-Bereitstellungen erfordern keine umfangreiche Hardware, was es Käufern ermöglicht, die meisten der IT-Aufgaben, die normalerweise für die Fehlerbehebung und Wartung der Software im eigenen Haus erforderlich sind, auszulagern. SaaS-Systeme werden in der Regel im Rahmen eines Abonnementmodells bezahlt, wohingegen Software vor Ort in der Regel über eine unbefristete, im Voraus bezahlte Lizenz erworben wird. 


**Verkauf von Support**
Bei diesem Modell kann das System von Unternehmen frei genutzt werden. Zusätzlich gibt es jedoch die Möglichkeit Support für die Nutzung und Integration des Systems zu kaufen. Das Unternehmen, das das System zur Verfügung stellt kann so die richtige und sichere Nutzung für den Käufer garantieren. Außerdem können diese bei Problemen kontaktiert werden. Dieses Prinzip wird meist über Abonnementmodelle ausgelegt. 

**Freemium**
Das Freemium-Modell stell das Open-Source-System ebenfalls kostenfrei zur Verfügung. In diesem sind alle Basic Funktionen enthalten.  Erweiterungen, wie Module, Plugins, Addons oder Themes können zusätzlich erworben werden. Dabei muss jedoch ein professionelles Geschäftsmodell entwickelt werden, welches lizenzkonforme Erweiterungen enthält. 
Außerdem muss bei Freemium zwischen freier und vollständig funktionierender Software und kostenloser, aber nur beschränkt nutzbarer Software unterschieden werden. 


**Spendenbasierte Finanzierung** (Crowdfunding)
Beim sogenannten Crowdfunding können Projekte durch Spenden einer Vielzahl von Interessierten finanziert werden. Dabei müssen keine großen Investoren akkreditiert werden, die Finanzierung wird durch Spenden und Leihgaben vieler Privatpersonen abgedeckt. Auf verschiedenen Websiten wie [Indigogo](www.indigogo.com) können Interessierte ihr Projekt anwerben und über diese Geldspenden erhalten.


**Partnerschaft mit Förderorganisation**
Bei diesem Modell können Organisation um ein Open-Source Projekt eine Partnerschaft mit einer Förderorganisation eingehen um die Finanzierung des Projekts zu decken. Diese Förderorganisationen können verschiedene Ziele haben und so z.B. über Drittmittel die Organisation bezahlen. In den meisten Fällen müssen solche Partnerschaften ein gemeinsames Ziel verfolgen (z.B. Wissenschaftlichen Aspekt) und beantragt werden.

Um eine Entscheidung für eins dieser Modelle zu treffen, sollten Organisationen die verschiedenen Eigenschaften abwegen und sämtliche Vor- und Nachteile eingehend betrachten. 


**Quellen**

[opensource.com, Moodle will always be an open source project, https://opensource.com/education/14/10/open-access-learning-moodle, last accessed: 23.03.20]

[phoronix, Towards A Real Business Model For Open-Source Software, https://www.phoronix.com/scan.php?page=article&item=sprewell_licensing&num=1, last accessed: 23.03.20]

[Software Advice, What is SaaS?, https://www.softwareadvice.com/resources/saas-10-faqs-software-service/, last accessed: 23.03.2020]
