---
layout: page
title: Dokumentation
permalink: /docs/
---

# Dokumentation

Willkommen zur Dokumentation des {{ site.title }} Projekts! Nutzen Sie folgende Quick-Links, um die Dokumentation zu erkunden.

<div class="section-index">
    <hr class="panel-line">
    {% for post in site.docs  %}        
    <div class="entry">
    <h5><a href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a></h5>
    <p>{{ post.description }}</p>
    </div>{% endfor %}
</div>
