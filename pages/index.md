---
layout: landingpage
permalink: /
---

## Willkommen beim Shopsystem Q!

Landwirtschaftliche Betriebe im Bergischen Land produzieren marktfähige und regionale Produkte in großer Vielfalt. Jedoch verfügen Sie weder über den Marktzugang noch über die Voraussetzung, diesen gegebenenfalls erfolgreich zu gestalten. Die Voraussetzungen umfassen Marketing- und Vertriebsknowhow, Infrastruktur, räumliche Lage und Verbrauchernähe. Außerdem sind die landwirtschaftlichen Betriebe nicht in der Lage, eine für den Lebensmitteleinzelhandel notwendige konstante Lieferfähigkeit aufrechtzuerhalten.

> Das Ziel von Projekt Q ist eine lokale, direkte Vermarktung von Erzeugnissen von Landwirten an Verbraucher zu realisieren. Als Mittel der Vermarktung wurde ein Webshop entwickelt, welcher den Anforderungen der Domäne entspricht. Projekt Q ist mit einer Konzeptions-, Entwicklungs- und Verwertungsdauer von drei Semestern aus einem studentischen Projekt entstanden. Es wurde mithilfe der Absprache mit verschiedenen Bauernvereine entwickelt.

{% include featurette.html
    image_left=true
    image_path="logo.png"
    image_alt="Q Logo"
    headline="Produkteinstellung simplifiziert"
    text="Das Erstellen von Produkten mit Q ist durch Vorlagen und eine automatische Bilderkennung für den Nutzer des Systems einfach und intuitiv möglich.<br />
    Bei der Erstellung kann aus einer Auswahl von Vorlagen gewählt werden. Diese können einzeln ausgewählt oder über eine automatische Bilderkennung gesucht werden. So kann ein Foto von einem Produkt, wie einer Banane, erstellt werden und es werden automatisch die passenden Daten ausgewählt."
    button_link="docs/user_manual#für-erzeuger"
%}

{% include featurette.html
    image_left=false
    image_path="docker_logo.svg"
    image_alt="Docker Logo"
    headline="Microservices und Docker"
    text="Q verwendet eine moderne Microservices-Architektur auf Basis von Docker."
    button_link="docs/erste-schritte"
%}
