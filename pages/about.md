---
title: Über Q
permalink: /about/
---

# Über Q

Landwirtschaftliche Betriebe im Bergischen Land produzieren marktfähige und regionale Produkte in großer Vielfalt. Jedoch verfügen Sie weder über den Marktzugang noch über die Voraussetzung, diesen gegebenenfalls erfolgreich zu gestalten. Die Voraussetzungen umfassen Marketing- und Vertriebsknowhow, Infrastruktur, räumliche Lage und Verbrauchernähe. Außerdem sind die landwirtschaftlichen Betriebe nicht in der Lage, eine für den Lebensmitteleinzelhandel notwendige konstante Lieferfähigkeit aufrechtzuerhalten.

> Das Ziel von Projekt Q ist eine lokale, direkte Vermarktung von Erzeugnissen von Landwirten an Verbraucher zu realisieren. Als Mittel der Vermarktung wurde ein Webshop entwickelt, welcher den Anforderungen der Domäne entspricht. Projekt Q ist mit einer Konzeptions-, Entwicklungs- und Verwertungsdauer von drei Semestern aus einem studentischen Projekt entstanden. Es wurde mithilfe der Absprache mit verschiedenen Bauernvereine entwickelt.

