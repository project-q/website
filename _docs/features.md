---
title: Features
tags: 
 - q
 - frontend
 - architecture
description: Features des Q Webshop
---

# Features

Die Features des Q-Webshops werden hier beschrieben.

## Architektur

![High-Level Interface Flow](https://user-content.gitlab-static.net/b61790d008fb6e55537f962ac4a6eaa8f218a06a/68747470733a2f2f692e696d6775722e636f6d2f764845587471522e6a7067)

[Legende:](https://seilevel.com/requirements/rml-model-user-interface-flow) ![Legende Flow](https://seilevel.com/requirements/wp-content/uploads/sites/2/2015/08/UI-Flow-Symbols.png)

Der oben gezeigte User-Interface-Flow zeigt einen umfassenden Überblick über die Benutzeroberfläche und deren Interaktionen. Dieses wurde während der Planung des Frontends entwickelt und ist aus den in den [User-Stories](https://gitlab.com/project-q/documentation/wikis/1/Prozessdokumentation-Tag-4-Donnerstag) ermittelten Verhaltensansichten der einzelnen Anwendungsfälle entstanden. Die dort entstandenen User-Stories boten konkrete Anwendungsfälle, die ein Nutzer des Webshops durchläuft. Das Ergebnis der Verbindung der in Projekt I erarbeiteten User-Stories ist nun in dieser Requirements Modeling Language-Architekturansicht (RML) der Benutzeroberfläche zusammengefasst.
 
Das Diagramm zeigt dabei bereits die Seiten und Interaktionen, die bis Meilenstein 2 erreicht werden sollen. In Meilenstein 1 haben die Consumer Interaktionen rund um den Shop und die Produkteinstellung und Verwaltung der Producer die höchste Priorität.

Beginnend mit einer funktionierenden Registrierung der beiden User-Typen Consumer und Producer sowie der Login, der nach erfolgreicher Validierung auf das Dashboard für die Producer bzw Shoppage für Consumer verweist, zeigt das Diagramm zu welchen Pages durch welche User-Trigger navigiert werden kann.

## Technologien

Als Basistechnologie wurde für die Umsetzung des Frontends die [React-Bibliothek](https://reactjs.org) verwendet. ReactJS ist eine sehr beliebte JS-Bibliothek, die von Facebook entwickelt wurde. Die Grundlage von React ist Javascript XML (JSX), eine Javascript-Extension, die die Erweiterung um Elemente aus XML (Extensible Markup Language) ermöglicht.
React bringt viele Vorteile mit sich, zum Einen das virtuelles DOM (Document-Objekt-Modell), das einfacher zu manipulieren ist als übliche DOMs, welches dabei eine schnelle Umsetzung von Interaktionen ermöglicht. Außerdem bietet React Entwicklern großen Freiraum bei z.B. der Kombination von Bibliotheken. React basiert auf der Erstellung und dem Design von Komponenten. Dies wurde auch bei der Umsetzung des Frontends verwendet. 
Die Dateistruktur des Codes spielt mit den im Backend verwendeten Microservices zusammen. Dabei wurde für jeden Microservice, der eine Fachlichkeit des Webshops vertritt, ein Ordner erstellt. In diesem, wiederum durch Ordner getrennt, werden Komponenten und Pages für die einzelnen Seiten definiert. React bietet mit dem Update 16.8. die Verwendung von React [Hooks](https://reactjs.org/docs/hooks-intro.html). Diese ermöglichen die Nutzung von States ohne React Classes zu erstellen. Bei der Entwicklung des Frontends wurde dieses neue Konzept genutzt. 

### Hilfsbibliotheken 

**create-react-app**

[Create React App](https://github.com/facebook/create-react-app) ist ein, ebenfalls von Facebook entwickeltes Hilfsframework, welches bei der Erstellung einer ersten Anwendungsstruktur und der Konfiguration dieser hilft. Es unterstützt den Entwickler durch einfache Befehle eine Datenstruktur zu erstellen und bietet verschiedene Tools wie Syntax-Support oder Autoprefixes für CSS. 

**React Router**

Zur Unterstützung im Bereich Routing wird [React Router](https://github.com/ReactTraining/react-router) verwendet. Diese Bibliothek bietet eine Sammlung von Navigationskomponenten, die deklarativ zur Erstellung der Anwendung genutzt werden können. Im Projekt wurde die Bibliothek dazu genutzt, URLs für die Webanwendung zu organisieren.

**Apollo React Hooks**

Die von [Apollo](https://www.apollographql.com/docs/react/api/react-hooks/) zur Verfügung gestellte API bietet verschiedene logische Funktionen, welche die Kommunikation zwischen Frontend und Backend regeln. Dabei werden React Hooks angeboten, die z.B. für das Einbinden von GraphQL-Queries in das Frontend zuständig sind.

**Image Gallery**

Für die Image Gallery auf der Productpage wurde eine externe Library von [logtype](https://gitlab.com/logotype/image-gallery-react) verwendet, die eine skalierbare Image Gallery für mehrere Bilder bietet. 

**JSON-Web-Token**

[JSON-Web-Token](https://jwt.io) bieten die Möglichkeit, über Accesss Token, die Identifizierung und Autorisierung von definierten User-Gruppen zu organisieren. Weiterhin bietet es die Möglichkeit Stateless Sessions einzuführen, bei der Informationen über Nutzer nicht auf dem Server selbst gespeichert werden müssen.

### Design
Für das Design der wiederverwendbaren und nicht wiederverwendbaren Komponenten wurde übergreifend Inline-CSS genutzt. Für standardisierte Parameter, wie Layout, Constraints, Farben und Schriften wurde eine Theme-Datei angelegt, auf die die einzelnen Komponenten zurückgreifen konnten. Auch Breakpoints wurden in externen .CSS Dateien definiert.
Für den weiteren Verlauf des Projekts wird die Strukturierung des [Atom-Designs](http://bradfrost.com/blog/post/atomic-web-design/) angezielt. Dabei sollen Atome und Moleküle weiterhin in Inline-CSS verbaut und für Organismen und Templates eigene strukturierte CSS-Dateien angelegt werden.

**EmotionCore**

[EmotionCore](https://emotion.sh/docs/introduction) bietet eine Bibliothek zum Schreiben von CSS-Stilen mit JavaScript. Dabei bietet es zusätzliche Funktionen wie Komponenten oder Testtools.

**SASS**

[SASS](https://sass-lang.com) (Syntactically Awesome Style Sheets) ist eine Stylesheet-Sprache und CSS-Präprozessor, die es ermöglicht CSS um verschiedene Funktionen und Variablen zu erweitern. Im Projekt wurde SASS für projekt-übergreifende Style-Dateien genutzt.

**React Bootstrap**

[React Bootstrap](https://react-bootstrap.github.io/) ist wohl eines der beliebtesten Frontend Frameworks und erweitert das Bootstrap für Javascript um Komponenten und Styles für ReactJS. Für ein responsive Webdesign wurde in vielen Komponenten auf die einfache Verwendung von Selektoren zurückgegriffen, die in Bootstrap als Wrapper genutzt werden. Die vielen Komponenten, wie Buttons, NavigationsBars und Cards werden über den Webshop verteilt verwendet und durch einfache Inline-Styles erweitert. 

**Font-Awesome**

Die Hilfsbibliothek [Font-Awesome](https://fontawesome.com) bietet verschiedene Icons zur freien Verfügung an, die über Prefixes im CSS direkt editiert werden können. Diese finden Anwendung im der Produkterstellung.


## Product Templates

Mithilfe des *Product Templates*-Features können Erzeuger Basisvorlagen oder eigene individuelle Vorlagen für die Produkteinstellung verwenden und speichern. 
Dieser Service soll zur schnelleren und einfacheren Produkteinstellung beitragen.

## Product Recognition

Ähnlich dem *Product Templates*-Features bekommt der Erzeuger passende Vorlagen zu dem Produkt, welches auf dem von ihm erzeugten Bild erkannt wurde. Hat der Product-Recognition Microservice zum Beispiel eine Erdbeere erkannt, wird dem Erzeuger alle Basisvorlagen zu diesem Produkt und alle individuellen Vorlagen dazu angezeigt. Sollte der Microservice kein Produkt erkannt haben, oder es gibt keine Vorlagen für das erkannte Produkt, wird der Erzeuger auf die manuelle Einstellung einens Produktes geleitet.

## Account Management

Die Account-Komponente überprüft bei der Erstellung und Aktualisierung die Validität der eingegeben Daten.

Die Auth-Komponente führt die Authentisierung am Gateway durch und überprüft die eingegebenen Zugangsdaten vor dem Absenden auf Validität, beispielsweise ob eine korrekte Mail-Adresse eingegeben wurde. Außerdem werden die bei der Registrierung angegebenen Daten auf Validität überprüft.

## Producer Dashboard

Die Dashboard-Komponente ermöglicht dem Erzeuger die Verwaltung seines Shops. Dabei werden bei der Produkterstellung Templates bereitgestellt und es wird die Validität der eingegebenen Daten überprüft.

## Producer Profile

Die Produkt-Komponente enthält keine Anwendungslogik, sie stellt lediglich die Profilseiten der Erzeuger dar.

## Shopping Cart

Die Shopping Cart Komponente ermögicht das Bearbeiten des Warenkorbs und wickelt den gesamten Bestellprozess des Kunden ab. Hierbei werden die eingegebene Daten auf Validität überprüft.
