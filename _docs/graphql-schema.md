---
title: GraphQL Schema
layout: page_large
tags: 
 - q
 - graphql
 - architecture
description: Schemata der GraphQL-API von Q
---

# GraphQL Schema

<iframe src="https://project-q.gitlab.io/graphdocs/" style="border: none; width:100%; height:80vh;"></iframe>
