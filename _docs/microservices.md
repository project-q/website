---
title: Microservices
tags: 
 - q
 - microservices
 - architecture
description: Microservices des Q Webshop
---

# Microservices

Die Microservices des Q-Webshops werden hier beschrieben.

## Architektur

Im Folgenden wird die übergreifende System-Architektur des Backends beschrieben. Dadurch sollen Zusammenhänge des Q-Gateway und der weiteren Microservices verdeutlicht werden. In diesem Kontext werden die Interna des Q-Frontends nicht aufgeführt (Details in [Features](./features)). 

![Architecture](https://gitlab.com/project-q/documentation/-/wikis/2/images/backend-system-design/architecture.png)

Die Microservices mit gestrichelten Rändern sind geplant, werden in dieser Phase jedoch noch nicht entwickelt.

Bei der Analyse der Domäne wurden zwei primäre Akteure festgestellt: Consumer und Producer. Zwei weitere Akteure könnten bei der Weiterentwicklung des Systems berücksichtigt werden: Admin und Pickup-Manager.

Alle Microservices besitzen GraphQL-Schnittstellen und können vom Q-Gateway angesteuert werden. Externe APIs, wie zum Beispiel die vorgesehene Stock Image API, könnte ein abweichendes Schnittstellen-Paradigma besitzen (REST, SOAP o. Ä.).

## Technologien

Bei der Implementierung des Backends wurden einige Hilfsbibliotheken verwendet und bestimmte Stärken von Technologien ausgenutzt.

### JWT
[jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken) wurde gewählt, da es das stateless-Konzept verfolgt und mit Nutzlast versehen werden kann. Zudem kann dieses Token mittels eines Ablaufdatums zeitlich begrenzt werden. 
Dieses Projekt verwendet JWT zur Autorisierung eines Benutzers.

Nach Authentifizierung des Benutzers wird mithilfe der _sign_-Funktion ein Token generiert, mit welchem sich der Benutzer vortan autorisieren kann.

```javascript
const token = jwt.sign(
    { 
        id: authenticatedAccount._id, 
        accountType: authenticatedAccount.accountType, 
        issuedAt: new Date() 
    }, 
        process.env.AUTH_KEY || AUTH_KEY, 
        { algorithm: 'HS256'})
```

Die Autorisierung mittels JWT-Token findet bei jeder graphQL-Anfrage im Q-Gateway statt.
```javascript
function getUser(token) {
    try {
      if (token) {
        return jwt.verify(token, process.env.AUTH_KEY || AUTH_KEY)
      }
        ...
```

### shortid
[shortid](https://www.npmjs.com/package/shortid) ist eines der etabliertesten ID-Generatoren und eignet sich hervorragend als Ersatz für die von mongoDB erzeugten _\_id's_. Shortid selbst garantiert eine massenhafte Generierung von eindeutigen ID'S.

```javascript
userInput._id = shortid.generate() //Example: DMXGE1q5 or 54TcG-ID
```

### bcrypt
[bcrypt](https://www.npmjs.com/package/bcryptjs) ist ein Hashing-Verfahren, welches speziell für Passwörter entwickelt wurde. Es wurden lediglich 10 Rounds verwendet, der Algorithmus hasht das Passwort also 10 mal. Mehr Rounds sind sicherer, aber benötigen mehr Rechenleistung für eine ähnliche Geschwindigkeit.

```javascript
// cleartext password gets hashed and overwritten by the hashed value
accountInputWithId.password = await new Promise((resolve, reject) => {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(accountInputWithId.password, salt, function (err, hash) {
            if (err) reject(err)
            resolve(hash)
        })
    })
})

```

```javascript
// given password gets compared with the password of the account the user wants to log in
const passwordVerified = await new Promise((resolve, reject) => {
    bcrypt.compare(accountInput.password, authenticatedAccount.password, function (err, res) {
        if (res) {
          resolve(res)
        } else {
          resolve(err)
          }
    })
})

```

### Base64
[Base64](https://docs.python.org/3/library/base64.html) ist ein Verfahren, bei dem binäre Daten in einen ASCII String encodiert werden. Dieser String kann dann übermittelt und gepseichert werden, bis er wieder decodiert wird. Diese Technologie wird zum decodieren von Bildern verwendet, um diese dann weiter zu verarbeiten.

```python
# decodiert den encodedMedia String zu einem Bild
imgdata = base64.b64decode(encodedMedia)
```


### ImageAi
[ImageAI](https://github.com/OlafenwaMoses/ImageAI) ist eine Bibliothek in Python, welche verschiedene Möglichkeiten im Bereich Objekterkennung mit Hilfe von neuronalen Netzen bietet. Schon mit wenigen Zeilen lässt sich so ein adäquates Netz starten, welches die trainierten Objekte erkennt. Anwendung findet diese Technologie bei der Erkennung von Produkten eines Erzeugers.

```python
# gibt das erkannte Objekt und die Wahrscheinlichtkeit dafür an
predictions, percentage_probabilities = prediction.predictImage(input_path, result_count=1)
```


## Gateway
Das Gateway ist selbst auch ein Microservice, hat aber kein eigenes GraphQL-Schema beziehungsweise Resolver. Es fügt die Schemata und Resolver der anderen Microservices zusammen und dient als Zugangspunkt zum System von Clients, wie zum Beispiel dem Frontend. Während der Zusammenführung überprüft es Schemata-Konflikte und gibt Lösungsvorschläge, oder weißt darauf hin. Das Gateway leitet alle GraphQL Anfragen an die jeweiligen Microservices weiter und stellt deren Antwort wiederrum nach außen bereit.

Der Gateway Microservice ist ein zentraler Dienst und die erste Kommunikationsschnittstelle für das Frontend. Es wird eine Service-Liste mit den einzelnen Microservices gepflegt mit den jeweiligen Namen und URLs. Bei der Ausführung des Gateways wird zu Beginn die Erreichbarkeit der einzelnen Microservices aus der Service-List überprüft und anschließend werden die GraphQL-Schemata und -Resolvers der erfolgreich verbundenen Microservices zusammengestellt. Das Gateway verifiziert außerdem eingehende JWT Tokens vom Frontend und reicht den dekodierten Token-Payload an die anderen Services weiter, damit diese nicht auch jeweils eine Logik zum Dekodieren von Tokens besitzen müssen.

## Product
Der Microservice Q-Product ist ein zentraler Bestandteil des Systems, da über diesen alle Funktionen zu den Produkten realisiert werden.

[Q-Product Schema](https://gitlab.com/project-q/q-product/blob/master/src/schemas/schemas.graphql)

Erzeuger haben durch eine große Anzahl von Metadaten viele Möglichkeiten, ihre Produkte ausführlich zu kennzeichnen. Durch diese semantische Modelierung können die Verbraucher die Produkte im Shop effizienter filtern.

Folgend sieht man zum einen den Typ Product, der viele Metadaten in Form von Enums beinhaltet.

```graphql
type Product @key(fields: "_id") {
    _id: ID!
    producerID: String!
    name: String!
    createdAt: DateTime!
    description: String!
    longDescription: String
    offerTypes: [OfferType!]!
    brand: String
    rating: Float
    category: Category!
    allergen: [Allergen!]
    additive: [Additive!]
    foodSafety: [FoodSafety!]
    color: String
    packaging: [Packaging!]
    condition: [Condition!]
    images: [Image!]!
}
```

Hier einmal das Enum Condition, welches einige mögliche Zustände eines Nahrungsmittels enthällt:

```graphql
enum Condition {
    MOIST
    FROZEN
    COOLED
    DRIED
    RTP
    RIPE
    RAW
    FERMENTED
}
```

Der Typ Product bietet auch die Möglichkeit, verschiedene Angebotstypen anzugeben. 
```graphql
type OfferType{
    price: Float!
    amount: Float!
    unit: Unit
    tax: Float!
    discount: Float!
}
```

Product verfügt derzeit über verschiedene Filterfunktionen (z.B. Produkte nach Kategorie filtern). Der Service agiert als zentraler Speicher für alle Produkte des Webshops und ermöglicht die Erstellung, Aktualisierung und das Löschen von einzelnen Produkten.

## Product Template
Um die Produkterstellung zu vereinfachen, können Product-Templates verwendet werden. Hier gibt es 2 Typen, die _Basis-Templates_, welche von dem Seitenbetreiber beziehungsweise dem Admin zur Verfügung gestellt werden, und die _Specific-Templates_, welche von den Erzeugern erstellt werden können, um zum Beispiel wiederkehrende Produkte nicht jedesmal neu anlegen zu müssen.

[Q-ProductTemplate Schema](https://gitlab.com/project-q/q-producttemplate/blob/master/src/typeDefs.js)

Die Typen dieses Microservices haben die Gleiche Datenstruktur wie die des Produkt-Services.

```graphql
 type BasisTemplate @key(fields: "_id"){
    _id: ID!
    title: String!
    description: String!
    longDescription: String
    ...
  }
```

Es wird in diesem Service zwischen zwei Arten von Templates unterschieden: Basis und spezifische Templates. Die Erstellung, Bearbeitung und das Löschen der jeweiligen Arten erfolgt lediglich, wenn der Benutzer eine Admin-Rolle besitzt oder eine Producer-Rolle besitzt im Fall von spezifischen Templates. Darüber hinaus können die Template-Arten nach ID gefiltert werden und nach Producer im Fall von spezifischen Templates.

## Account
Verwaltet die Informationen von Benutzern und stellt Account-Funktionen bereit. Zudem kommuniziert Q-Account mit Q-Auth bei der Registrierung eines neuen Benutzers.

[Q-Account Schema](https://gitlab.com/project-q/q-account/blob/master/src/schemas/schemas.graphql)


```graphql
input CreateConsumerInput {
        name: String!
        surname: String!
        password: String!
        email: String!
        address: AddressCreateInput!
        type: UserType!
}
```
Bei der Registrierung wird das Passwort mit übergeben, da Q-Account direkt mit Q-Auth kommuniziert und das Passwort zum hashen und speichern übergibt.

```graphql
type ProducerProfile {
        producerProfileId: ID!
        name: String!
        address: Address!
        accountMedia: [AccountMedia!]
        publicEmail: String
        telephone: [Int!]
        openingHours: OpeningHours
        visitPossible: Boolean
        averageRating: Float
        AcceptedPaymentMethods: [PaymentMethod!]!
}
```
Dieser Typ ist speziell für die Anzeige des Producers im Webshop bei den einzelnen Produkten. Diese Informationen sind öffentlich sichtbar.

Der Account Service ist zuständig für die Erstellung, Bearbeitung und das Löschen von Konten. Es unterscheidet derzeit die Benutzer in drei unterschiedlichen Rollen: Consumer, Producer und Admin. Abhängig vom User-Typ werden bei der Erstellung von Konten bestimmte Eigenschaften dem Benutzer zugewiesen. Beispielsweise wird dem Erzeuger ein Attribut Steuer-ID zugeordnet. Der Account Service speichert bei der Kontenerstellung kein Passwort, sondern leitet es weiter an den Auth Service, um die Sicherheit beziehungsweise die Integrität der Daten zu gewährleisten. Sollte die Account Datenbank von unerlaubten Personen kompromittiert werden, so wären die gehashten Passwörter dennoch geschützt. 

## Auth
Die Hauptaufgabe dieses Microservices ist das hashen und speichern des User-Passwortes, sowie zur Authentifizierung und Autorisierung mittels eines Token.

[Q-Auth Schema](https://gitlab.com/project-q/q-auth/blob/master/src/schemas/schemas.graphql)

Um sich als Benutzer zu authentisieren, wird folgende Query verwendet:

```graphql
authenticateAccount(accountInput: AccountInput) : AuthAccount
```

Der Input dieser Query ist die Email-Addresse und das Passwort. Wenn beide zusammenpassen, wird ein _authToken_ erstellt und als Antwort zurürckgegeben.

```graphql
input AccountInput {
    email: String!
    password: String!
}

type AuthAccount {
    authToken: String!
}
```

Auth dient primär zur Authentisierung der Benutzer. Es wird also ein Token generiert, sobald ein Benutzer mit seinen korrekten Anmeldedaten einloggt. Wenn neue Konten im Account Service angelegt werden, ist der Auth Service dafür zuständig die Anmeldedaten, also E-Mail und Passwort des Benutzers zu hinterlegen. Darüber hinaus bietet der Auth Service berechtigte Benutzer und den Admins auch die Möglichkeit das persönliche Passwort zu modifizieren.

## Shopping Cart
Sobald ein eingeloggter Benutzer ein Produkt bestellen möchte, legt er dieses in den Warenkorb. Jeder Benzuter kann nur einen Warenkorb haben, dieser wird erstellt, sobald ein Benutzer sich registriert und gelöscht, wenn der Account gelöscht wird.
Zu jedem Produkt im Warenkorb wird auch der Erzeuger gespeichert, da dieser während des Bestellprozesses wichtig ist.

[Q-ShoppingCart Schema](https://gitlab.com/project-q/q-shoppingcart/blob/master/src/schemas/schemas.graphql)

```graphql
type ShoppingCart {
    _id: ID!
    consumerId: ID!
    products: [ShoppingCartProduct!]
}


type ShoppingCartProduct {
    productId: ID!
    productName: String!
    producerId: ID!
    amount: Float!
    price: Float!
    tax: Float!
    image: String!
}
```
Auch während dem Bestellprozess bleibt der Warenkorb solange erhalten, bis die Bestellung kostenpflichtig abgeschlossen wurde.

Der Warenkorb-Service ist ein wesentlicher Bestandteil des Webshops. Shopping Cart ist verantwortlich für Funktionen, um die Shopping Cart zu erstellen, Produkte hinzuzufügen, Produktmengen zu ändern, die Shopping Cart zu leeren oder löschen. Die Shopping Cart soll zukünftig auch ohne Anmeldung verwendbar sein. Derzeit können die Funktionen, um die Shopping Cart zu bearbeiten, nur durch berechtigte Benutzer oder Admins ausgelöst werden. Ein spezifisches Produkt kann der Shopping Cart nur einmalig hinzugefügt werden. Wenn eine größere/kleinere Menge von diesem Produkt benötigt wird, dann geschieht dieses über eine eigene Funktion (ModifyProductAmount). Wird die Menge eines Produkts durch die ModifyProductAmount Funktion gesetzt, so wird das Produkt von der Shopping Cart gelöscht.

## Order
Da ein Verbraucher von mehreren Erzeugern Produkte bestellen kann, wird nach Abschluss der Bestellung für jeden Erzeuger, von dem ein oder mehrere Produkte gekauft wurden, eine Bestellung erzeugt. Somit kann jeder Erzeuger individuell über eine eingehende Bestellung informiert werden, ohne dass dieser Informationen über andere Erzeuger erhält, bei denen ein Verbraucher eventuell noch bestellt hat.
Damit diese Bestellungen in der Bestellüberischt des Verbrauchers als eine Bestellung angezeigt werden können, muss bei der Erzeugung eine _groupId_ generiert und der Bestellung hinzugefügt werden.

[Q-Order Schema](https://gitlab.com/project-q/q-order/blob/master/src/schemas/schemas.graphql)

Die Rechnung für jede Bestellung wird im Backend erstellt und für den Verbraucher im Frontend bei Bedarf konkateniert.

```graphql
type Order @key(fields: "_id"){
    _id: ID!
    consumer: ID!
    producer: ID!
    groupId: ID!
    products: [OrderedProduct!]!
    total:Float!
    pickupStation: PickupStation!
    date: DateTime!
    orderStatus: [OrderStatus!]!
    paymentMethod: PaymentMethod!
    bill: Bill!
}

type OrderStatus{
    status: Status!
    date: DateTime!
}
```
Das Feld orderStatus ist ein Array, da zum aktuellen Status auch das Datum gepspeichert wird.

Bestellungen, die nach der zahlungspflichtigen Bestellung von Warenkorbprodukten erstellt werden, werden im Order Service verwaltet. Zur Erstellung einer Bestellung werden verschiedene Informationen benötigt, wie zum Beispiel die Erzeuger- und Verbraucheranschrift. Das bedeutet, dass die createOrder Mutation mit verschiedenen Microservices kommuniziert. Es wird außerdem überprüft, dass die Produkte aus einem Warenkorb als gruppierte Bestellung gespeichert werden, sodass diese zu einem späteren Zeitpunkt gruppiert abgerufen werden können (vom Frontend). Sobald eine Bestellung erstellt wurde, wird auch auf den Topic 'addedOrder' gepublished.

## Media

Der Media Microservice ist für das Hochladen und das Löschen von Medien beziehungsweise Bilder verantwortlich. Bilder können von beliebigen Microservices hochgeladen werden, die Bilder in Base64 kodieren können. Als Object Storage verwendet dieser Microservice Firebase Cloud Storage, mit Server innerhalb Europa. Vor der Speicherung in dem Cloud Storage wird das Eingabebild auf drei verschiedenen Auflösungen skaliert. Die öffentlichen URLs der skalierten Bilder werden vom Microservice zurückgeliefert. In der Datenbank von Q-Media werden lediglich der Name und das Format des Eingabebilds und die öffentlichen URLs der skalierten Bilder gespeichert.  

```graphql
type Media {
    name: String!
    mediaURLs: [String!]!
    contentType: String!
}
```

## Product Recognition

Dieser Microservice besteht aus zwei Teilen. Zum einen eine NodeJS Server, welche die GraphQL Schnittstelle zur Verfügung stellt und zum anderen ein neuronales Netz in Python. Dieses neuronale Netz decodiert ein in Base64 übertragenes Bild und versucht zu erkennen, um welches Objekt es sich handelt. Als neuronales Netz kommt die Python Bibliothek ImageAI zum Einsatz. Diese Bibliothek bringt ein Modell mit, welches auf 1000 verschiedene Objekte trainiert wurde. Als Input benötigt ImageAi ein Bild, welches mittels Pfadangabe übergeben wird. Dadurch muss das decodierte BIld zunächst gepseichert und nach der Erkennung wieder gelöscht werden.

```python
# Speichere das übertragene und decodierte Bild (imgdata) unter dem aktuellen Timespamp
filename = f'{time.time()}.jpg'
with open(filename, 'wb') as f:
f.write(imgdata)
```

Base64 bietet keine Möglichkeit zu überprüfen, ob es sich bei dem decodierten BIld um ein valides Bild handelt. Deshalb ist die prediction, bei der das zuvor gespeicherte Bild geladen wird, in einen try/catch Block und wirft eine Fehlermeldung, sollte das Bild nicht valide sein. Hier wird auch das zwischengespeicherte Bild wieder gelöscht.

```python
try:
    predictions, percentage_probabilities = prediction.predictImage(
        input_path, result_count=1)
    os.remove(filename)
    if math.floor(percentage_probabilities[0]) > 85:
        print(f'{str(predictions[0])}')
except (IOError, OSError, ValueError) as err:
    os.remove(filename)
    print(f'Error: {err}')
```