---
title: Roadmap
description: Interaktive Roadmap für das Projekt Q
---

# Roadmap

Die interaktive Roadmap stellt den strategischen Plan von Projekt Q anhand der Issues dar und beschreibt Ziele, Schritte und Meilensteine, die erreicht werden müssen, damit der Plan realisiert werden kann.

<style>
    .legend{
        display: none;
    }
    .y-axis{
        display: none;
    }
    .grp-axis, .group-tooltip{
        display: none!important;
    }
</style>

<div id="roadmapViz">

</div>


<script src="//unpkg.com/timelines-chart"></script>
<script>

    const projectName = 'Project Q'
    const projectUrl = 'https://gitlab.com/api/v4/groups/project-q'
    const settings = {
        resultsPerPage: 100,
        state: 'all',
        scope: 'all',
        utf8: '✓'
    }
    const roadmapData = [
        {
            group: projectName,
            data: []
        }
    ]

    initRoadmap()

    async function initRoadmap() {
        const issues = await fetchIssues()
        console.log(issues)
        for (let i = issues.length-1; i >= 0; i--) {
            roadmapData[0].data.push({
                label: issues[i].title,
                data: [{
                    timeRange: [
                        issues[i].created_at,
                        issues[i].closed_at || new Date()
                    ],
                    val: `Author: ${issues[i].author.name}`
                }]
            })
        }

        const gitLabRoadmap = TimelinesChart()

        gitLabRoadmap.data(roadmapData)('#roadmapViz').width(window.innerWidth-parseInt(window.innerWidth*0.35))
        resizeRoadmap(gitLabRoadmap, roadmapData)

    }

    async function fetchIssues() {
        return await fetch(`${projectUrl}/issues?per_page=${settings.resultsPerPage}&utf8=${settings.utf8}&state=${settings.state}&scope=${settings.scope}`).then((response) => {
            return response.json();
        }).then((data) => {
            return data
        });
    }

    function resizeRoadmap(gitLabRoadmap, roadmapData){
        window.addEventListener('resize', () => {
            document.getElementById('roadmapViz').innerHTML = ''
            gitLabRoadmap.data(roadmapData)('#roadmapViz').width(window.innerWidth-parseInt(window.innerWidth*0.35))    
        })
    }

    

</script>
