---
title: Häufige Probleme
tags: 
 - q
 - issues
description: Sammlung von bekannten Issues und entsprechenden Lösungen
---

# Häufige Probleme

### Hinzufügen eines Produkts (als Erzeuger)
**Inputs:**
 - Das vom Benutzer während der Hinzufügung gewählte `Foto` des Produkts wird nicht angezeigt (das Standardfoto der Vorlage wird bei Verwendung einer Vorlage angezeigt, und es wird kein Foto angezeigt, wenn keine Vorlage bereit ist).
 - *Zusatzstoffe/Lebensmittelsicherheit/Allergien* : Die Bestätigung der Auswahl `<select>` wird nicht angezeigt `(UI)`

**Vorlagen**
 -  Anzeigeproblem bei der Auswahl der Vorlage.
 - Der Erzeuger kann die Vorlage nicht speichern  und wieder verwenden .

**Fehler**
 - Fehler bei der Auswahl einer Produktvorlage angezeigt.  `Uncaught TypeError: Cannot read property '0' of undefined`
 - ![Unbenannt](https://user-images.githubusercontent.com/15965492/77674601-22e8ad00-6f8c-11ea-8660-61e2a0591c6f.PNG)



### Account
**Inputs:**
 - Falsh input types (Hausnummer)
 
**Fehler : 400 (Bad Request)**
 - [https://mivs01.gm.fh-koeln.de/account](https://mivs01.gm.fh-koeln.de/account) 
 
 ![Unbenannt2](https://user-images.githubusercontent.com/15965492/77675228-f41f0680-6f8c-11ea-99ec-964a6d287f57.PNG)


### Bestellung
 - Der Erzeuger kann sein eigenes Produkt bestellen (in den Warenkorb legen).
 - Bestellungen bei Verbraucher und Erzeuger funktionieren nicht.
