---
title: Erste Schritte
tags: 
 - q
 - einrichtung
description: Erste Schritte mit dem Q Webshop
---

# Erste Schritte

Auf dieser Seite wird beschrieben, wie mit Q gestartet und wie die Microservices-Architektur über Docker ausgeführt werden kann.

## Docker Umgebung

Alle Microserives von Q sind als Docker-Container entwickelt, sodass lediglich Docker (mit Docker Compose) installiert sein muss, um die Umgebung zu starten.

Um zu beginnen, musst das Repository "[development](https://gitlab.com/project-q/development)" geklont oder heruntergeladen werden. Dort muss in den `docker-environment`-Ordner gewechselt und eine Datei mit dem Namen `secrets.env` erstellt werden. Diese kann vorerst leer bleiben, sie muss aber existieren. Jetzt kann die Umgebung bereits mit Docker Compose gestartet werden. Dafür wird ein Terminal geöffnet und in den `docker-environment`-Ordner des Repositories gewechselt. Nun kann die Umgebung per `docker-compose up` gestartet werden. Dies wird beim ersten Mal etwas dauern, da alle Docker Images zunächst heruntergeladen werden müssen. Anschließend sollte per `http://localhost:8080` auf das Frontend zugegriffen werden können. Die Ports der einzelnen Services sind [unten](#services-und-docker-ports) aufgelistet.

Um das System im Hintergrund auszuführen, können foldende Befehle verwendet werden:

- `docker-compose up -d` zum Erstellen (und Starten) im Hintergrund (`-d`)
- `docker-compose stop` zum Stoppen
- `docker-compose start` zum Starten
- `docker-compose restart` zum Neustarten
- `docker-compose down` zum Löschen des Systems verwenden

Um die Docker Images zu aktualisieren, kann `docker-compose pull` ausgeführt werden, anschließend sollte das System per `docker-compose up -d` mit den aktualisierten Images neu erstellt werden.  
Sollten nicht alle Services korrekt funktionieren, kann es helfen das Gateway während alle anderen Services laufen, einmal per `docker-compose restart gateway` neu zu starten.

### Services und Docker Ports

Auf jeden Service sollte über das Gateway zugegriffen werden, aber für Entwicklungszwecke wird auch jeder Service über einen eigenen Port freigegeben.  
**Hinweis:** Diese Ports werden über einen Docker an das Host-System freigegeben und entsprechen nicht den intern verwendetet Ports. Ebenfalls sind diese Ports aus Sicherheitsgründen nur über `localhost` freigegeben und nicht über das Netzwerk erreichbar. Jeder Service sollte in seinem Container auf Port 3000 laufen.

| **Service** | **Port** |
| ------ | ------ |
| [frontend](https://gitlab.com/project-q/q-frontend) | 8080 |
| [gateway](https://gitlab.com/project-q/q-gateway) | 4000 |
| [product](https://gitlab.com/project-q/q-product) | 3000 |
| [product-template](https://gitlab.com/project-q/q-producttemplate) | 3001 |
| [shopping-cart](https://gitlab.com/project-q/q-shoppingcart) | 3002 |
| [order](https://gitlab.com/project-q/q-order) | 3003 |
| [account](https://gitlab.com/project-q/q-account) | 3004 |
| [auth](https://gitlab.com/project-q/q-auth) | 3005 |
| [media](https://gitlab.com/project-q/q-media) | 3006 |
| [image-recognition](https://gitlab.com/project-q/q-imagerecognition) | 3007 |
