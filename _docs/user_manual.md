---
title: Benutzerhandbuch
tags: 
 - q
 - manual
description: Handbuch für Verbraucher, Erzeuger und landwirtschaftliche Vereine.
---

# Benutzerhandbuch

In diesem Benutzerhandbuch von Q werden zwei Versionen, die aktuelle Version und die geplante Benutzeroberfläche, erläutert.

## Benutzerhandbuch für die aktuelle Version des Systems

Das aktuelle System bietet Zugang für zwei Arten von Nutzern. 
Zum einen für Verbraucher und zum anderen für Erzeuger von Lebensmitteln.

#### Startseite

Die Startseite von Q besteht aus Informationen zur der Nutzung von Q und der Abfrage einer Postleitzahl, um die Produktsuche für das jeweilige Gebiete einzuschränken.

![Startscreenplz](https://user-images.githubusercontent.com/49209272/78231012-7732e600-74d2-11ea-9e09-357f0129f533.png)

Auf der Produktseite befindet sich die Menüleiste, Produkte die derzeit angeboten werden und ein Filterbereich, 
in dem die Produkte nach verschiedenen Produktkategorien, Preisen und Bewertungen 
gefiltert werden können. Über die Menüleiste ist es möglich zum Erzeugerbereich zu gelangen, 
sich anzumelden oder zu registrieren und den Warenkorb einzusehen.

![Startscreen](https://user-images.githubusercontent.com/49209272/78231238-c9740700-74d2-11ea-8bb5-312cb94f3e39.png)

#### Login

Damit Nutzer (bereits registrierte Erzeuger und Verbraucher) sind anmelden können, muss die E-Mail-Adresse und das dazugehörige Passwort eingegeben werden.
Besitzen Nutzer noch kein Konto bei Q gibt es die Möglichkeit über den *registrieren*-Button zum Formular für die Registrierung zu gelangen.

![Login](https://user-images.githubusercontent.com/49209272/78231886-c299c400-74d3-11ea-8bdc-d24ea71b3c5e.png)

### Für Verbraucher

#### Produktdetails

Über die Produktseite, in der die verfügbaren Produkte angezeigt werden, können die Detailinformationen der 
Produkte über ein Klick auf das Produkt angezeigt werden. Über die Produktdetailseite können Produkte auch dem Warenkorb hinzugefügt werden. Die detailierte Produktansicht enthählt aktuell Informationen wie:
*  Produktinformationen
*  Produktdetails
*  Erzeuger des Produktes

Produkte können jedoch aktuell nicht, ohne angemeldet zu sein, dem Warenkorb hinzugefügt werden und auch nicht gekauft werden.

![Produktdetails](https://user-images.githubusercontent.com/49209272/78232472-992d6800-74d4-11ea-92a0-6988a68d0674.png)

Ist ein Nutzer in Q angemeldet, wird der *Warenkorb*-Button aktiv und kann angeklickt werden.

![Produktdetails2](https://user-images.githubusercontent.com/49209272/78232261-45228380-74d4-11ea-8e65-b7d64df49bff.png)


#### Als Verbraucher registrieren

Um sich als Verbraucher registrieren zu können, sind alle geforderten Informationen notwendig. Darüberhinaus sollte die Option *Consumer* ausgewählt werden, um ein Verbraucherkonto anzulegen.

![RegisterConsumer](https://user-images.githubusercontent.com/49209272/78234001-d09d1400-74d6-11ea-979f-54752729fd17.png)

#### Warenkorb

Über den *Warenkorb*-Button der Produktdetailseite werden die Produkte dem Warenkorb hinzugefügt, in dem die Produkt gelistet werden und ebenfalls bearbeitet und entfernt werden können.
Um eine Bestellung abzuschließen, sollte der *Bestellung abschicken*-Button betätigt werden.

Diese Abbildung zeigt den Warekorb ohne gelistete Produkte.

![Shoppingcardempty](https://user-images.githubusercontent.com/49209272/78234242-2c679d00-74d7-11ea-98ad-6b371e29113d.png)

Diese Abbildung zeigt den Warekorb mit gelisteten hinzugefügten Produkten.

![Shoppingcardwithproduct](https://user-images.githubusercontent.com/49209272/78234428-6c2e8480-74d7-11ea-9983-18cf35a28e64.png)

#### Einstellungen

Zu den Einstellungen des Profil gelangen Nutzer über das *Profil*-Icon in der Menüleiste. Im Profil werden die Daten des registrierten Nutzers aufbewahrt, die je nach belieben geändert werden können.
Um Datenänderungen des Profils zu speichern, muss nach der Änderung der *Änderung speichern*-Button betätigt werden.

![Settingsaccount](https://user-images.githubusercontent.com/49209272/78234612-b1eb4d00-74d7-11ea-9795-fe984bb7e355.png)

Des Weiteren befinden sich in den Einstellungen die getätigten Bestellungen des Nutzers.

![Settingsorder](https://user-images.githubusercontent.com/49209272/78234805-f971d900-74d7-11ea-85e5-e7107167dd5c.png)

### Für Erzeuger

#### Als Erzeuger registrieren

Um sich als Erzeuger registrieren zu können benötigt es ebenfalls eine Registrierung, bei der die Anmeldungsoption als Producer gewählt werden muss.

![RegisterProducer](https://user-images.githubusercontent.com/49209272/78235073-5f5e6080-74d8-11ea-99d0-fd38ff925982.png)

#### Dasboard

Jeder Erzeuger besitzt ein Dashboard, über das die aktuell angebotenen Produkte verwaltet und neue Produkte angeboten werden können.
Über den *Produkt hinzufügen*-Button können weitere Produkte zum Verkauf angeboten werden.

![Dasbaord](https://user-images.githubusercontent.com/49209272/78235270-af3d2780-74d8-11ea-8562-39af3d22e3d5.png)

Bevor jedoch Produkte angeboten werden können ist die Eingabe von Daten eines Produktes notwendig.
Q stellt einige Basisvorlagen zur Verfügung, um die Dateneingabe zu minimieren und die Veröffentlichung von Produkt zu vereinfachen.
Q bietet den Erzeuger ebenfalls die Funktion, eigene individuelle Vorlagen zu erstellen.
Darüber hinaus können Vorlagen mit Hilfe einer Produkterkennung über die Kamera erkannt werden, um noch schneller auf Vorlagen zugreifen zu können. 

![Addproduct](https://user-images.githubusercontent.com/49209272/78235549-12c75500-74d9-11ea-9ef3-0865d4fefc0c.png)

Im Bereich der Basis-Vorlagen und der individuellen Vorlagen werden alle Vorlagen aufgeführt die verwendet werden können.
Um einen schnellen Gesamtüberblick über die jeweilige Vorlage zu erhalten, kann der *Detaillierte Schnellansicht*-Button betätigt werden, 
um alle Informationen der Vorlage anzuzeigen.

![Basistemplates](https://user-images.githubusercontent.com/49209272/78235737-5cb03b00-74d9-11ea-848c-793088939761.png)

Sind keine Vorlagen vorhanden, wird dies über die Nachricht "Keine Templates vorhanden" mitgeteilt.

![Notemplates](https://user-images.githubusercontent.com/49209272/78235856-88332580-74d9-11ea-858a-ecae586c96b4.png)

Die detaillierte Schnellansicht der Produktvorlage soll alle Informationen, die für die Produkteinstellung notwendig ist, auf einem Blick anzeigen.
Über diese Ansicht soll demnach auch die Produktveröffentlichung ermöglicht werden.

![Templatedetails](https://user-images.githubusercontent.com/49209272/78236059-cc262a80-74d9-11ea-8aa5-4e5885766ede.png)

Um Produktvorlagen über die Kamera zu erkennen, öffnet sich die Kamera des jeweiligen Einsatzgerätes, mit dem das jeweilige Produkt erkannt wird und die Produktvorlagen anzeigen soll.
Um ein Produkt erkennen zu können muss das Objekt erfasst werden, beziehungsweise ein Foto des Objektes gemacht werden.

![Produkttemplaterecognition](https://user-images.githubusercontent.com/49209272/78236306-13acb680-74da-11ea-97a8-fc74c4da40da.png)

Wird eine Vorlage für das ausfüllen von Daten verwendet, werden die Formularfelder bereits mit den Daten der Vorlage gefüllt.
Der Produkteinstellungsprozess besteht nach der Auswahl einer Vorlage oder keiner Vorlage aus fünf weitere Schritten.
Der zweite Schritt besteht aus der Benennung und der Beschreibung des Produktes 

![Publishproduct2](https://user-images.githubusercontent.com/49209272/78236622-8ae24a80-74da-11ea-9bdd-7d79af7c6665.png)

Im dritten Schritt können Bilder des jeweiligen Produktes hochgeladen werden, die für den Verkauf zur Veranschaulichung des Produktes für Verbraucher gesehen werden können.

![Publishproduct3](https://user-images.githubusercontent.com/49209272/78236846-dbf23e80-74da-11ea-9eea-2ddbc50cf96f.png)

Der vierte Schritt des Einstellungsprozesses erfordert Informationen der angebotenen Menge, der Mengeneinheit und des Preises pro Mengeneinheit.

![Publishproduct4](https://user-images.githubusercontent.com/49209272/78237288-689cfc80-74db-11ea-9a77-88d1c64f2719.png)

Um den Verbrauchern detaillierte Informationen über das Produk zu liefern, können im fünften Schritt weitere Informationen angegeben werden, wie:
*  Marke
*  Farbe
*  Zustand
*  Verpackung
*  Zusatzstoffe
*  Lebensmittelsicherheit
*  Allergen

![Publishproduct5](https://user-images.githubusercontent.com/49209272/78237404-9124f680-74db-11ea-99ad-dc5062c3eb7c.png)

Im sechsten und auch im letzten Schritt des Einstellungsprozesses werden alle Informationen des zu veröffentlichten Produktes zusammengefasst.
In diesem Zuge kann das Produkt über den *Produkt veröffentlichen*-Button für Verbraucher veröffentlicht und angeboten werden.

![Publishproduct6](https://user-images.githubusercontent.com/49209272/78237617-d8ab8280-74db-11ea-8f6d-6c4bfd0f93b5.png)


### Für landwirtschaftliche Vereine

## Benutzerhandbuch für die zukünftige Version des Systems

### Für Verbraucher und Erzeuger

#### Startseite

Für das zukünftige System oder die neue Version wurde ein neues Konzept für die Benutzeroberfläche 
entwickelt, damit Q ansprechender nach außen tritt. Das komplette Design, die 
Screens und der Prototyp können ebenfalls unter [Q-Frontend Design/Figma](https://www.figma.com/file/0XepZi7Klhgf6JL9qcHTCsFY/Q-Frontend?node-id=0%3A1) eingesehen werden.
Auch hier besteht der Zugang für zwei Arten von Nutzer. Zum einen für Verbraucher und zum anderen für Erzeuger von Lebensmitteln.

Grundlegend gelangen beide Arten von Nutzer auf die gleiche Startseite.
Besucht ein Nutzer den Shop zum aller ersten mal, dann wird nach der **Postleitzahl** gefragt, 
um die Produktauswahl und die Erzeuger von Lebensmittel einzugrenzen. Diese Postleitzahl 
wird daraufhin im Local Storage gespeichert, damit der Nutzer beim nächsten Aufruf des 
Shops direkt zur Produktansicht gelangt.

![Startscreen](https://user-images.githubusercontent.com/49209272/76340199-78218f00-62fb-11ea-9452-d30f50ba3e6a.png)

Befindet sich die Postleitzahl im Local Storage gelangt der Nutzer, egal ob Erzeuger oder Verbraucher, zur Produktansicht.
Auf der Startseite ,nach der Eingabe der Postleitzahl, befinden sich folgende Komponenten:
*  Navigation/Kategorieauswahl
*  Profil/Login/Registrierung
*  Warenkorb
*  Suche
*  Postleitzahl
*  Produktanordnungsfilter
*  Proddukte

Zusätzlich sind Produkte mit *distance marker* versehen, damit Produkte mit der geringsten Distanz zum Erzeuger aufgeführt werden.
Unter den nachfolgenden Punkten wird die Benutzeroberfläche jeweils für die bestimmte Art von Nutzer erläutert.

![Productview](https://user-images.githubusercontent.com/49209272/76340395-d0f12780-62fb-11ea-9f3c-a105cf876bfd.png)

#### Profil

Erzeuger und Verbraucher haben die Möglichkeit über die Startseite ihr Profil einzusehen, sich anzumelden oder sich zu registrieren. 
Im Profil der jeweiligen Nutzer werden Daten der Verbraucher und Erzeuger abgelegt, 
die für ein erfolgreichen Einkauf- und Verkaufsprozess notwendig sind.
Das Profil des jeweiligen Nutzers kann über die Produktstartseite aber auch über die Startseite, 
bei der noch keine Postleitzahl bekannt ist, eingesehen werden. Ist der jeweilige Nutzer noch nicht eingeloggt, erscheinen die drei Auswahlmöglichkeiten:
*  Einloggen
*  Registrieren als Verbraucher
*  Registrieren als Erzeuger

Wurde keine Postleitzahl eingegeben, steht dem Nutzer im Header ausschließlich die 
Profilseite zur Verfügung, was bedeutet, dass Nutzer nicht zur Produktseite gelangen können.

![Profil](https://user-images.githubusercontent.com/49209272/76340668-3e9d5380-62fc-11ea-8136-1c1eabaef5e8.png)

#### Einloggen

Für Erzeuger wie auch für Verbraucher erscheint die identische Benutzeroberfläche für das Login. 
Für das Login ist hierbei die Email-Adresse und das Passwort notwendig. 
Über diesen Weg ist es zusätzlich möglich zur Registrierung zu gelangen.

![Login](https://user-images.githubusercontent.com/49209272/76341167-f16db180-62fc-11ea-9b93-ff871de13aaf.png)

### Für Verbraucher

#### Produktkategorien

Verbraucher haben die Möglichkeit die Produkte nach verschiedenen Kategorien zu filtern. 
In der mobilen Ansicht erscheinen die Kategorien nach der Betätigung des Navigation-Symbols. 
In der Desktop-Ansicht befindet sich der Kategorienbereich bereits auf der Produktstartseite.

![ProductCategory](https://user-images.githubusercontent.com/49209272/76333280-b1550180-62f1-11ea-9b7f-add6fe708dfa.png)

Wurde eine Kategorie ausgewählt, wird die jeweilige Kategorie hervorgehoben und die Produkte der ausgewählten Kategorie angezeigt.

![SelectedProductCategory](https://user-images.githubusercontent.com/49209272/76342960-b4ef8500-62ff-11ea-823d-bf7f189f4f5e.png)

#### Registrieren

Zur Registrierung gelangen Nutzer (Verbraucher) über den Button *Account erstellen* im [Profil](#profil)

Für die Registrierung eines Nutzerkontos werden mindestens folgenden relevanten Informationen benötigt:
*  Vorname
*  Nachname
*  E-Mail-Adresse
*  Passwort

![RegisterConsumer](https://user-images.githubusercontent.com/49209272/76342048-3514eb00-62fe-11ea-8892-4633a9c4b5db.png)

#### Eingeloggter Verbraucher

Nach dem erfolgreichen Login kann der Nutzer seine Daten und Bestellungen einsehen und verwalten.
Des Weiteren kann der Nutzer wieder zur Startseite zurückkehren oder sich abmelden.

![LoggedConsumer](https://user-images.githubusercontent.com/49209272/76368980-3d871900-6332-11ea-920c-b1c26e1dbe89.png)

#### Warenkorb

Im Warenkorb werden alle Produkte aufbewahrt die zuvor hinzugefügt wurden.
Befinden sich keine Produkte im Warenkorb, erscheint diesbezüglich eine Meldung.

![EmptyShoppingCard](https://user-images.githubusercontent.com/49209272/76343548-8e7e1980-6300-11ea-92c6-8cb3f0f29424.png)

Wurden Produkte dem Warekorb hinzugefügt, werden diese aufgelistet. Diese können daraufhin geändert oder entfernt werden.
Soll der Einkauf fortgesetzt werden, kann der Button *zur Kasse* betätigt werden, wobei der Nutzer zur [Kasse](#kasse) weitergeleitet wird, um seinen Einkauf abzuschließen.

![FullShoppingCard](https://user-images.githubusercontent.com/49209272/76344744-5e377a80-6302-11ea-8f8c-5936b25209d9.png)


#### Kasse

Um die Checkout-Phase vollenden zu können werden zum einen die notwendigen Daten eingefordert und die jeweilige Bezahlmethode.

Ist der Nutzer nicht eingeloggt, sind folgende Daten erforderlich:
*  Vorname
*  Nachname
*  E-Mail-Adresse

Des Weiteren kann sich ein Nutzer auch hier einloggen, um die Dateneingabe zu umgehen, da diese Daten bereits im Profil hinterlegt sind.
Daraufhin stehen dem Nutzer zwei Möglichkeiten bereit wie die Produkte bereitgestellt werden können:
*  Direkt beim Erzeuger abholen
*  Zu einer Abholstation liefern lasse und die Produkte dort abholen

Will der Verbraucher die Produkte direkt beim Erzeuger abholen, wird dem Nutzer die Adresse des jeweiligen Erzeugers und die Entfernung zur angegebenen Postleitzahl angezeigt.

![Checkout1](https://user-images.githubusercontent.com/49209272/76346545-7f4d9a80-6305-11ea-9efc-d6abcc16df99.png)

Möchte der Nutzer die Produkte zu einer Abholstation liefern lassen, werden die verfügbaren in der nähe liegenden Abholstationen angezeigt, zu denen die Produkte geliefert werden können.

![Checkout2](https://user-images.githubusercontent.com/49209272/76347167-73aea380-6306-11ea-9481-7ae1af2cd040.png)

Nach der Datenerfassung wird der Nutzer zur Bezahlmethode weitergeleitet. Auch hier hat der Nutzer noch die Möglichkeit eingegebene Daten zu ändern.
Um den Einkauf abschließen zu können wird nach der gewünschten Bezahlmethode gefragt.

![Checkout3](https://user-images.githubusercontent.com/49209272/76347843-8e354c80-6307-11ea-9128-7c827c5dfcdc.png)

Wurde eine Bezahlmethode ausgewählt wird der *Bestellen*-Button aktiviert und kann betätigt werden, um die Bestellung abzuschließen.

![Checkout4](https://user-images.githubusercontent.com/49209272/76348655-d99c2a80-6308-11ea-8bc4-93d662ee3469.png)

Wurde die Bestellung erfolgreich abgeschlossen, erfolgt ein Hacken und eine Label um den Nutzer mitzuteilen, dass die Bestellung erfolgreich war.

![Checkout5](https://user-images.githubusercontent.com/49209272/76348801-18ca7b80-6309-11ea-880e-235ce4a163dc.png)


#### Produktsuche

Q bietet die Möglichkeit nach Produkten zu suchen. Für die Produktsuche wird ein Suchfeld bereitgestellt, in dem der jeweilige Suchbegriff eingegeben werden kann.

![ProduktSearch](https://user-images.githubusercontent.com/49209272/76349300-03a21c80-630a-11ea-9878-ce4c7692a67c.png)


#### Produktanordnung

Die Produkte können auch je nach Interessen unterschiedlich angeordnet werden. Standartmäßig werden die Produkte eines Erzeugers mit der geringsten Entfernung angezeigt.
Jedoch können die Produkte nach Preis, Erscheinungsdatum oder Bewertung angezeigt werden.

![ProductArrangement](https://user-images.githubusercontent.com/49209272/76349556-84f9af00-630a-11ea-9933-9313bb9e70e7.png)

#### Produktdetails

Jedes Produkt enthählt weitere detailreiche Informationen die durch einen Klick auf das Produkt eingesehen werden können.
Neben dem Produktbild und der Produktpreise befindet sich in der Produktdetailansicht eine detaillierte Beschreibung, Bewertungen zum Produkt und eine kurze Aufführung des Erzeugers.
Über die Produktdetailansicht kann das Produkt ebenfalls in den Warenkorb gelegt werden.

![Productdetails](https://user-images.githubusercontent.com/49209272/76356885-f6d7f580-6316-11ea-8aea-ab9820c7035c.png)

#### Erzeugerseite

Die Erzeugerseite enhält alle wichtigen Informationen über den ausgewählten Erzeuger. Neben einer Beschreibung des Erzeuger befinden sich auf dieser Seite auch alle Produkte und Kontaktinformationen des Erzeugers.

![Producerprofil](https://user-images.githubusercontent.com/49209272/76363170-ca29db00-6322-11ea-8d56-739d9052ac98.png)


### Für Erzeuger

#### Registrieren

Erzeuger können sich ebenfalls über das [Profil](#profil) registrieren, in dem sie den *Jetzt Erzeuger werden-Link* betätigen. Anschließend gelangen Erzeuger auf eine Erzeugerinformationsseite, 
die kurz etwas über den Verkauf von Lebensmittel erzählt.
Über den *Jetzt registrieren*-Link kann die Registrierung gestartet werden.

![RegisterProducerStep1](https://user-images.githubusercontent.com/49209272/76357932-e294f800-6318-11ea-9831-194a84192019.png)

Für die Registrierung eines Erzeugers werden einige Informationen benötigt, 
damit der Erzeuger sich bei den Verbrauchern vorstellen kann und die dafür notwendige Informationen bereitstellt.
Ebenfalls wird bei diesem Schritt ein Account für den Erzeuger mit der E-Mail-Adresse und das dazugehörende Passwort erstellt.

![RegisterProducerStep2](https://user-images.githubusercontent.com/49209272/76358187-5cc57c80-6319-11ea-99d2-80bf43ea6463.png)

#### Eingeloggter Erzeuger

Ist der Erzeuger eingeloggt, stehen ihm allgemeine Einstellungen aber auch ein Erzeugerbereich zur Produkt- und Bestellverwaltung zur Verfügung.
Der Erzeugerbereich ist durch die Betätigung des *Zu deinem Erzeugerbereich*-Buttons erreichbar.

![LoggedProducer](https://user-images.githubusercontent.com/49209272/76358800-5be11a80-631a-11ea-8a51-f5623581630f.png)

#### Erzeugerbereich - Produkt

Im Erzeugerbereich hat der Erzeuger die Möglichkeit, seine Produkt einzustellen, zu löschen oder zu ändern.
Zu Beginn werden dem Erzeuger Produkte angezeigt, die er derzeit anbietet. Diese Produkte kann der Erzeuger bearbeiten oder löschen.
Zusätzlich kann der Erzeuger hier neue Produkt einstellen.

![ProducerProductSection](https://user-images.githubusercontent.com/49209272/76360571-8a142980-631d-11ea-907a-68dbf3e7c86e.png)

Bietet der Erzeuger derzeit keine Produkte an, erscheint die Meldung *Du bietest derzeit keine Produkte an*.

![ProducerProductSection2](https://user-images.githubusercontent.com/49209272/76360977-4ff75780-631e-11ea-866f-2e2a1ced7cd7.png)

Möchte der Erzeuger ein neues Produkt einstellen, stehen ihm Vorlagen zur Verfügung, die er verwenden kann. Der Erzeuger hat die Möglichkeit 
vordefinierte Basisvorlagen oder auch zuvor angelegte individuelle Vorlagen zu verwenden.
Es ist aber auch möglich die Produkteinstellung ohne eine Vorlage zu verwenden durchzuführen. Für die Vereinfachung der Sucher steht dem Erzeuger 
eine Bilderkennung zur verfügung, mit der Vorlagen über ein Kamerabild gesucht und ausgewählt werden können.

![ProducerProductSection3](https://user-images.githubusercontent.com/49209272/77322545-2eca3a00-6d14-11ea-86cb-98406b6a7e82.png)

Wählt der Erzeuger die Option Basisvorlagen aus, werden Produkte angezeigt, die daraufhin verwendet werden können. 
Die Vorlagen können über den *Detaillierte Schnellansicht*-Button spezifischer betrachtet werden.

![ProducerProductSection4](https://user-images.githubusercontent.com/49209272/77322597-41dd0a00-6d14-11ea-8e12-21d36cf90864.png)

Über die Funktion zur Bilderkennung von Produkten können vorhandene Vorlage ausgewählt werden. 

![ProducerProductRecognition](https://user-images.githubusercontent.com/49209272/77353379-29371900-6d41-11ea-8eb9-2ca8704a35f1.png)

Die detaillierte Schnellansicht zeigt alle Informationen der Vorlage. 
Über die detaillierte Schnellansicht kann das Produkt für Datenänderungen verwendet werden aber auch direkt veröffentlicht werden.

![ProducerProductSection5](https://user-images.githubusercontent.com/49209272/77322645-528d8000-6d14-11ea-94ac-f3739ade0dd2.png)

Unabhängig ob eine Vorlage verwendet wird oder nicht, werden die nächsten Schritte durchlaufen. Wurde keine Vorlage verwendet sind die Formularfelder leer.
Der Erzeuger muss dem Produkt einen Namen und eine Beschreibung geben. Alternativ kann eine detaillierte Beschreibung folgen.

![ProducerProductSection6](https://user-images.githubusercontent.com/49209272/76361723-e1b39480-631f-11ea-816d-72b8d7d15402.png)

Anschließend muss der Erzeuger Bilder zum jeweiligen Produkt hochladen, damit diese für Verbraucher anschaulich sind.

![ProducerProductSection7](https://user-images.githubusercontent.com/49209272/76361912-47a01c00-6320-11ea-9695-9710320ed201.png)

Daraufhin bestimmt der Erzeuger die Menge und den Preis pro Menge.

![ProducerProductSection8](https://user-images.githubusercontent.com/49209272/76362024-8930c700-6320-11ea-8785-913347dca4e3.png)

Zum Schluss der Dateneingabe muss der Erzeuger weitere detaillierte Zusatzinformationen über das Produkt angeben, welches er veröffentlichen möchte.

![ProducerProductSection9](https://user-images.githubusercontent.com/49209272/77316053-f2dda780-6d08-11ea-8eaa-4fd066599525.png)

Bevor das Produkt veröffentlicht wird und zum Verkauf angeboten wird, bekommt der Erzeuger eine Zusammenfassung des Produktes. 
Das Produkt kann hier noch geändert werden. Das Produkt kann ebenfalls als Vorlage gespeichert werden, wo es dann bei den 
individuellen Vorlagen angezeigt wird und wiederverwendet werden kann.
Das Produkt kann über den *Produkt einstellen*-Button veröffentlich werden und wird danach zum Verkauf angeboten

![ProducerProductSection10](https://user-images.githubusercontent.com/49209272/76362335-2b50af00-6321-11ea-8332-969697f58973.png)


#### Erzeugerbereich - Bestellungen

Im Erzeugerbereich hat der Erzeuger die weitere Möglichkeit die getätigten Bestellungen der Verbraucher einzusehen

![ProducerOrderSection1](https://user-images.githubusercontent.com/49209272/76362625-9ef2bc00-6321-11ea-9ee3-5b3bed9d392d.png)

![ProducerOrderSection2](https://user-images.githubusercontent.com/49209272/76362813-07da3400-6322-11ea-8195-f71675773c5c.png)


### Für landwirtschaftliche Vereine

<!-- Preflight und Setup andeuten: kann in der Zukunft zur Vereinfachung der Prozesse implementiert werden, wenn ein Verein das System lokal aufsetzen sollte. -->