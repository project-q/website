---
title: Mitwirken
tags: 
 - q
 - contribute
description: Am Projekt Q mitwirken
---

# Mitwirken

Projekt Q ist ein Open-Source-Projekt. Jegliche Beiträge beziehungsweise Verbesserungen sind willkommen und werden von unserem Team unterstützt. Unsere [Roadmap](./roadmap) bietet einen Überblick der aktuellen Issues und kann als Anhaltspunkt verwendet werden, falls Ideen für Features vorhanden sind.

## Dokumentation

Die aktuelle Dokumentation von Projekt Q wird vom [Cayman Theme (Jekyll)](https://pages-themes.github.io/cayman/) repräsentiert. Neue Features oder Verbesserungen an der aktuellen Dokumentation können auf zwei Arten vorgenommen werden. Die einfachste Variante besteht darin einzelne Seiten auf [project-q.gitlab.io](https://project-q.gitlab.io/website/docs/) manuell zu bearbeiten. Alternativ kann das [Repository](https://gitlab.com/project-q/website) geclont werden und mit Jekyll lokal ausgeführt werden.

```shell
bundle exec jekyll serve
```

Die Prozess- und System-Dokumentation der Konzeption und Entwicklung von Projekt Q wurden im [Dokumentation Wiki](https://gitlab.com/project-q/documentation/-/wikis/home) festgehalten. In diesem Wiki können wichtige Entscheidungen, welche die Entwicklung des Webshops beeinflusst haben nachvollzogen werden.

## Microservices

Die Mehrheit der Microservices von Projekt Q sind in NodeJS geschrieben. Die Weiterentwicklung des bestehenden Microservices erweist sich als einfach, wenn NodeJS-Kenntnisse vorliegen. Das Testen der Services in lokaler Umgebung kann mithilfe von `docker compose` stattfinden oder dem manuellen Ausführen von einzelnen Microservices. Es ist empfehlenswert Microservices immer über das Gateway zu testen, da dieses eventuelle Konflikte in GraphQL-Schemata überprüft und meldet. Soll ein neuer Microservice entwickelt werden, so muss im [Development Repository](https://gitlab.com/project-q/development) die `docker-compose.yml` ergänzt werden. Dazu sollten die in der [README.md](https://gitlab.com/project-q/development/-/blob/master/README.md) definierten Regeln zur Port-Vergabe berücksichtigt werden.

## Frontend

Das Frontend wird in React entwickelt und wurde auf Grundlage von `Create React App` strukturiert und architektonisch aufgebaut. Einzelne Seiten im Frontend entsprechen grob den einzelnen Microservices. Es gibt außerdem sogenannte Shared Components, welche von mehreren Komponenten genutzt werden. Wenn aktuellen Komponenten überarbeitet werden oder neue Komponenten entwickelt werden, kann die [Anleitung](https://gitlab.com/project-q/q-frontend/blob/master/README.md) hilfreich für die Strukturierung sein. Für Naming-Konventionen der Komponenten können die bereits existierenden Komponenten als Anhaltspunkt verwendet werden.

## Internationalisierung

Projekt Q wurde für den deutschen Markt konzipiert. Eine Maßnahme für die Internationalisierung kann die Lokalisierung verschiedener Zielgruppen verbessern. Es gilt in diesem Kontext ein Konzept zu entwickeln, um Projekt Q zu internationalisieren und zugänglicher zu machen. Die Internationalisierung sollte es ermöglichen, verteilte Inhalte zu aggregieren, sodass diese später einfach übersetzt werden können. Es existieren bereits eine Vielzahl von `I18N-Frameworks`, welche als Grundlage verwendet werden können.
